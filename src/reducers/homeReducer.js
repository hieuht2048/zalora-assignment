import { fromJS } from 'immutable';
import * as types from '../constants/actionTypes';

const initialState = fromJS({
    msgs: [],
    sending: false,
    sendMessagesErr: false,
});

export default function(state = initialState, action) {
    switch (action.type) {
        case types.SEND_MESSGAGES:
            return state
                .set('msgs', fromJS(action.msgs))
                .set('sending', true)
                .set('sendMessagesErr', false);
        case types.SEND_MESSAGES_SUCCESS:
            return state.set('sending', false).set('sendMessagesErr', false);
        case types.SEND_MESSAGES_ERROR:
            return state.set('sendMessagesErr', action.error).set('sending', false);
        default:
            return state;
    }
}
