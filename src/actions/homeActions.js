import * as types from '../constants/actionTypes';

export const sendMessages = msgs => {
    return {
        type: types.SEND_MESSGAGES,
        msgs,
    };
};

export const sendMessagesSuccess = () => ({
    type: types.SEND_MESSAGES_SUCCESS,
});

export const sendMessagesError = error => ({
    type: types.SEND_MESSAGES_ERROR,
    error,
});
