import React, { PureComponent } from 'react';
import { Helmet } from 'react-helmet';
import { Router, Route, Switch } from 'react-router-dom';

import Header from '../common/Header';
import NotFound from '../common/NotFound';

import Routes from './routes';

class Routers extends PureComponent {
    render() {
        const { history } = this.props;
        return (
            <Router history={history}>
                <div className="main-container">
                    <Helmet titleTemplate="%s - Assignment" defaultTitle="Assignment">
                        <meta name="description" content="Assignment" />
                    </Helmet>
                    <Header />
                    <div className="main-content">
                        <Switch>
                            {Routes.length && Routes.map((route, key) => <Route key={key} {...route} />)}
                            <Route component={NotFound} />
                        </Switch>
                    </div>
                </div>
            </Router>
        );
    }
}

export default Routers;
