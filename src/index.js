import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';

import Routers from './routers';
import configureStore, { browserHistory } from './store';
import * as serviceWorker from './serviceWorker';

// Add CSS
import './styles/index.scss';

ReactDOM.render(
    <Provider store={configureStore()}>
        <Routers history={browserHistory} />
    </Provider>,
    document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister();
