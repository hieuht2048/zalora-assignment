import { store } from '../store';
import { sendMessagesError } from '../actions/homeActions';

class Core {
    splitMessage(str, lineLenght) {
        const totalLenght = str.length;
        const numLine = Math.ceil(totalLenght / lineLenght);
        // eslint-disable-next-line
        String.prototype.splice = function(idx, rem, str) {
            return this.slice(0, idx) + str + this.slice(idx + Math.abs(rem));
        };
        // check line number of the string, if longer than 1 line, add line number before of each line
        if (numLine > 1) {
            for (let line = 0; line < numLine; line++) {
                const placeInsert = str.charAt(line * lineLenght - 1);
                // check place to insert count, insert if it is a whitespace, throw an error if it is not.
                if (/^\s*$/.test(placeInsert)) {
                    str = str.splice(line * lineLenght, 0, line + 1 + '/' + numLine + ' ');
                    store.dispatch(sendMessagesError(false));
                } else {
                    const error = 'Error: Your message contains a span of non-whitespace characters longer than 50 characters';
                    store.dispatch(sendMessagesError(error));
                    return [];
                }
            }
        }
        const inputArray = str.split(' '); // convert string to array: "I can't believe Tweeter now" => ['I', 'can't', 'believe', 'Tweeter', 'now', 'supports']
        const inputArrayLen = inputArray.length;
        let formattedStr = '';
        let curLen = 0;
        for (let x = 0; x < inputArrayLen; x++) {
            if (curLen + inputArray[x].length < lineLenght) {
                formattedStr += ' ' + inputArray[x]; // join next word to array if the lenght of string is not enough (lineLenght).
                curLen += inputArray[x].length + 1; // increase the current length of string
            } else {
                formattedStr += '\n' + inputArray[x]; // down the line of the string if the length of the string is longer than limit (lineLenght)
                curLen = inputArray[x].length; // now, current length is equal length of the first word of next line
            }
        }
        formattedStr = formattedStr.split('\n'); // convert to array after '\n'
        return formattedStr;
    }
}

export default new Core();
