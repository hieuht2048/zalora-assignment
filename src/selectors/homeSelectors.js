import { createSelector } from 'reselect';

const homeSelect = state => {
    return state.homeReducer;
};

const makeSelectMessages = () =>
    createSelector(
        homeSelect,
        state => state.get('msgs')
    );

const makeSelectSendMessagesError = () =>
    createSelector(
        homeSelect,
        state => state.get('sendMessagesErr')
    );

export { makeSelectMessages, makeSelectSendMessagesError };
