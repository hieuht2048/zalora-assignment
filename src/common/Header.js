import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';

class Header extends Component {
    render() {
        return (
            <header className="header">
                <div className="logo">Assignment - HUYNH TRUNG HIEU</div>
            </header>
        );
    }
}

Header.propTypes = {};

const mapStateToProps = () => {
    return {};
};

const mapDispatchToProps = {};

export default withRouter(
    connect(
        mapStateToProps,
        mapDispatchToProps
    )(Header)
);
