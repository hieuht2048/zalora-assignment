import { createStore } from 'redux';
//import logger from 'redux-logger';
import createBrowserHistory from 'history/createBrowserHistory';

import rootReducer from '../reducers';

export const browserHistory = createBrowserHistory();

export const store = createStore(rootReducer);

//  Returns the store instance
// It can  also take initialState argument when provided
const configureStore = () => {
    return {
        ...store,
    };
};

export default configureStore;
