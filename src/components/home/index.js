import React, { Component } from 'react';
import { createStructuredSelector } from 'reselect';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import { ButtonBase, Grid } from '@material-ui/core';

import Messages from './Messages';
import Core from '../../Core';
import { sendMessages } from '../../actions/homeActions';
import { makeSelectMessages, makeSelectSendMessagesError } from '../../selectors/homeSelectors';

class HomePage extends Component {
    constructor(props) {
        super(props);
        this.state = {
            error: null,
            msg: [],
        };
    }

    onEnter = e => {
        const { msg } = this.state;
        const { onSendMessages, msgs } = this.props;
        const input = document.getElementById('msgInput');
        if (e.key === 'Enter') {
            if (msg.length <= 0) {
                e.preventDefault();
                this.setState({ error: 'Please enter your message!' });
            } else {
                const msgsUpdate = msgs.toJS();
                msgsUpdate.push(msg);
                onSendMessages(msgsUpdate);

                //reset state and form
                input.value = '';
                setTimeout(() => {
                    this.setState({ msg: [], error: null });
                }, 200);
            }
        }
    };

    onSend = () => {
        const { msg } = this.state;
        const { onSendMessages, msgs } = this.props;
        const input = document.getElementById('msgInput');
        if (msg.length <= 0) {
            this.setState({ error: 'Please enter your message!' });
        } else {
            const msgsUpdate = msgs.toJS();
            msgsUpdate.push(msg);
            onSendMessages(msgsUpdate);

            //reset state and form
            input.value = '';
            setTimeout(() => {
                this.setState({ msg: [], error: null });
            }, 200);
        }
    };

    checkingMsg = e => {
        const message = e.target.value;
        const msgLineLength = 50;
        let msg = [];
        if (message.length > msgLineLength) {
            msg = Core.splitMessage(message, msgLineLength);
        } else {
            msg = [message];
        }
        this.setState({ msg });
    };

    render() {
        const { error } = this.state;
        const { msgs, sendMessagesErr } = this.props;
        return (
            <Grid container>
                <Grid container className="main">
                    <Messages msgs={msgs} />
                </Grid>
                <Grid container className="input">
                    <Grid container className="input-error">
                        {sendMessagesErr ? sendMessagesErr : error}
                    </Grid>
                    <Grid container className="input-form">
                        <input
                            id="msgInput"
                            type="text"
                            placeholder="Enter your message..."
                            onChange={this.checkingMsg}
                            onKeyPress={e => this.onEnter(e)}
                        />
                        <ButtonBase className="btn btn-normal btn-green" onClick={this.onSend}>
                            <i className="fas fa-long-arrow-alt-right" />
                        </ButtonBase>
                    </Grid>
                </Grid>
            </Grid>
        );
    }
}

HomePage.propTypes = {
    /* eslint-disable */
    onSendMessages: PropTypes.func,
    msgs: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
    sendMessagesErr: PropTypes.oneOfType([PropTypes.bool, PropTypes.string]),
};

HomePage.defaultProps = {};

const mapStateToProps = createStructuredSelector({
    msgs: makeSelectMessages(),
    sendMessagesErr: makeSelectSendMessagesError(),
});

function mapDispatchToProps(dispatch) {
    return {
        onSendMessages: msgs => {
            dispatch(sendMessages(msgs));
        },
    };
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(HomePage);
