import React, { Component } from 'react';
import { createStructuredSelector } from 'reselect';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

//import CircularProgress from '@material-ui/core/CircularProgress';
import { Grid } from '@material-ui/core';

class Messages extends Component {
    render() {
        const { msgs } = this.props;
        const messages = msgs.toJS();
        return (
            <Grid className="messages">
                {messages.length > 0 ? (
                    messages.map((msg, index) => (
                        <div className="msg-row-wrp" key={index}>
                            <div className="avatar">
                                <i className="fas fa-user-circle" />
                            </div>
                            <div className="arrow-left" />
                            <div className="msg-row">
                                {msg.map((txt, indexTxt) => (
                                    <div key={indexTxt} className="msg">
                                        {txt}
                                    </div>
                                ))}
                            </div>
                        </div>
                    ))
                ) : (
                    <div className="no-mgs">Have no any message</div>
                )}
            </Grid>
        );
    }
}

Messages.propTypes = {
    /* eslint-disable */
    sending: PropTypes.bool,
    msgs: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
};

Messages.defaultProps = {};

const mapStateToProps = createStructuredSelector({});

const mapDispatchToProps = () => {
    return {};
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Messages);
